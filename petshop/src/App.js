import "./App.css";
import { BrowserRouter, Routes , Route } from "react-router-dom";
import Home from "./pages/Home";
import Productos from "./pages/Producto";
import Admin from "./pages/Admin";
import Dashboard from "./pages/Dashboard";
import Proveedor from "./pages/proveedor/Proveedor";
import AddProveedor from "./pages/proveedor/AddProveedor";
import ShowProveedor from "./pages/proveedor/ShowProveedor";
import ListaProducto from "./pages/producto/ListaProducto";
import AddProducto from "./pages/producto/AddProducto";
import ShowProducto from "./pages/producto/ShowProducto";
import ListaUsuario from "./pages/usuario/ListaUsuario";
import AddUsuario from "./pages/usuario/addUsuario";
import Login from "./pages/Login";
import Registro from "./pages/Registro";
import Producto from "./pages/Producto";
import ShowUsuario from "./pages/usuario/ShowUsuario";


function App() {
  return (
    <>
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} >
              <Route index element={<Producto />} />
              <Route path="login" element={<Login />} />
              <Route path="registro" element={<Registro />} />
          </Route>
          <Route path="/admin" element={<Admin />}>
              <Route index element={<Dashboard />} />
              <Route path="proveedor" element={<Proveedor />} />
              <Route path="proveedor/nuevo" element={<AddProveedor />} />
              <Route path="proveedor/:id" element={<ShowProveedor />} />
              <Route path="proveedor/update/:id" element={<AddProveedor />} />
              <Route path="producto" element={<ListaProducto />} />
              <Route path="producto/nuevo" element={<AddProducto />} />
              <Route path="producto/:id" element={<ShowProducto />} />
              <Route path="producto/update/:id" element={<AddProducto />} />
              <Route path="usuario" element={<ListaUsuario />} />
              <Route path="usuario/nuevo" element={<AddUsuario />} />
              <Route path="usuario/:id" element={<ShowUsuario />} />
              <Route path="usuario/update/:id" element={<AddUsuario />} />
          </Route>
          <Route path="/producto" element={<Productos />} />
          

        </Routes>
    </BrowserRouter>
    </>
    
  );
}

export default App;
