import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/usuario/registro";

const Registro = () => {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    usuario:"",
    clave: "",
    nombres: "",
    apellidos: "",
    tipoDoc: "",
    numDoc: "",
    direccion: "",
    telefono: "",
    email: ""
  });

  const { clave, nombres, apellidos, email } = formData;

  /** manejo de inputs */
  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  /** Guardar proveedor */
  const saveUsuario = (e) => {
    e.preventDefault();
    axios.post(baseURL, formData).then((response) => {
      setFormData(response.data);
    });
    navigate("/login");
  };

  return (
    <div className="login-form">
      <form className="form row" onSubmit={saveUsuario}>
        <h2 className="text-center">Crear una Cuenta</h2>
        <div className="col-md-6 form-group mb-2">
          <input
            type="text"
            className="form-control"
            placeholder="nombres"
            required="required"
            name="nombres"
            value={nombres}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-6 form-group mb-2">
          <input
            type="text"
            className="form-control"
            placeholder="apellidos"
            required="required"
            name="apellidos"
            value={apellidos}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-12 form-group mb-2">
          <input
            type="email"
            className="form-control"
            placeholder="Email"
            required="required"
            name="email"
            value={email}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-12 form-group mb-2">
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            required="required"
            name="clave"
            value={clave}
            onChange={handleChange}
          />
        </div>
        <div className="form-group mb-2">
          <button type="submit" className="btn btn-primary btn-block">
            Registro
          </button>
        </div>
      </form>
      <Link to={"/login"} className="text-center">
        {" "}
        Inicia sesion{" "}
      </Link>
    </div>
  );
};

export default Registro;
