import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/producto";

const ListaProducto = () => {
    const [productos, setProductos] = useState([]);

    useEffect(() => {
      getProducto();
    }, []);

      /** Validamos que el usuario este logueado */
  const authUser = () => {
    const user = JSON.parse(localStorage.getItem('token'));

    if (user && user.token) {
      return {Authorization: 'Bearer ' + user.token}
    } else {
      return {};
    }
  }
  
    /** lista de productos */
    const getProducto = () => {
      axios.get(baseURL).then((response) => {
        setProductos(response.data);
      });
    };
  
    /* eliminar productos */
    const deleteProducto= (e, id) => { 
      axios.delete(`${baseURL}/${id}`, {headers: authUser()}).then(
        (borrado) => {
          if (borrado) {
            getProducto();
          }
        }
      )
    }

  return (
    <div className="pt-4 p-2 table-responsive">
      <h2>
        Lista de productos
        <Link to={"/admin/producto/nuevo"}>
          <button type="button" className="btn btn-primary sm float-end">
            Nuevo
          </button>
        </Link>
      </h2>
      <hr />
      <br />
      <table className="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>UN/Medida</th>
            <th>Precio Venta</th>
            <th>Categoria</th>
            <th style={{ width: "15%" }}>acciones</th>
          </tr>
        </thead>
        <tbody>
          {productos.map((producto, index) => (
            <tr key={producto._id}>
              <td>{index + 1}</td>
              <td>{producto.codigo}</td>
              <td>{producto.nombre}</td>
              <td>{producto.unidadMedida}</td>
              <td>{producto.valorVenta}</td>
              <td>{producto.categoria}</td>
              <td>
                <div className="float-end">
                  <Link to={`/admin/producto/${producto._id}`}>
                    <button type="button" className="btn btn-success btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-eye `}></i>
                    </button>
                  </Link>
                  <Link to={`/admin/producto/update/${producto._id}`}>
                    <button type="button" className="btn btn-primary btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-pencil `}></i>
                    </button>
                  </Link>

                  <button type="button" className="btn btn-danger btn-sm" onClick={(e) => deleteProducto(e, producto._id)}>
                    <i className={`fas fa-trash `}></i>
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListaProducto;