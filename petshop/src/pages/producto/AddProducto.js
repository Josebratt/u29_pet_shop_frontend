import React, { useState, useEffect } from "react";
import { useNavigate, Link, useParams } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/producto";

const AddProducto = () => {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    nombre: "",
    codigo: "",
    unidadMedida: "",
    valorCompra: "",
    valorVenta: "",
    cantidad: "",
    categoria: "",
  });

  /** Validamos que el usuario este logueado */
  const authUser = () => {
    const user = JSON.parse(localStorage.getItem('token'));

    if (user && user.token) {
      return {Authorization: 'Bearer ' + user.token}
    } else {
      return {};
    }
  }

  // const [nombre, setNombre] = useState("");
  // const [codigo, setCodigo] = useState("");
  // // const [imageFile, setImageFile] = useState("");
  // const [unidadMedida, setUnidadMedida] = useState("");
  // const [valorCompra, setValorCompra] = useState("");
  // const [valorVenta, setValorVenta] = useState("");
  // const [cantidad, setCantidad] = useState("");
  // const [categoria, setCategoria] = useState("");

  // const loadImage = (e) => {
  //   const image = e.target.files[0];
  //   setImageFile(image);
  //   // setPreview(URL.createObjectURL(image));
  // };

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      showProducto(id);
    }
  }, [id]);

  

  const showProducto = (id) => {
    axios.get(`${baseURL}/${id}`).then((response) => {
      setFormData(response.data);
      // setNombre(response.data.nombre);
      // setCodigo(response.data.codigo);
      // setUnidadMedida(response.data.unidadMedida);
      // setValorCompra(response.data.valorCompra);
      // setValorVenta(response.data.valorVenta);
      // setCantidad(response.data.cantidad);
      // setCategoria(response.data.categoria);
    });
  };

  const { nombre, codigo, unidadMedida, valorCompra, valorVenta, cantidad, categoria } =
  formData;
  // const formData = new FormData();
  // formData.append('nombre', nombre);
  // formData.append('codigo', codigo);
  // // formData.append('image',imageFile);
  // formData.append('unidadMedida', unidadMedida);
  // formData.append('valorCompra', valorCompra);
  // formData.append('valorVenta', valorVenta);
  // formData.append('cantidad', cantidad);
  // formData.append('categoria', categoria);

    /** manejo de inputs */
    const handleChange = (e) => {
      setFormData({ ...formData, [e.target.name]: e.target.value });
    };
  /** manejo del submit */
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!id) {
      saveProducto();
    } else {
      updateProducto(id, formData);
    }
  };

  /** Guardar proveedor */
  const saveProducto = () => {
    axios.post(baseURL, formData, {headers: authUser()});
    navigate("/admin/producto");
  };

  /** Actualizar Producto */
  const updateProducto = (id, formData) => {
    axios.put(`${baseURL}/${id}`, formData, {headers: authUser()});
    navigate("/admin/producto");
  };

  return (
    <div className="p-5">
      <h5>{!id ? "Crear Producto" : "Actualizar Producto"}</h5>
      <hr />
      <form className="row g-2 pt-3" onSubmit={handleSubmit}>
        {/* <div className="mb-3">
          <input className="form-control form-control-sm" 
            onChange={loadImage}
            type="file" />
        </div> */}
        <div className="col-md-4">
          <label htmlFor="codigo" className="form-label">
            Codigo
          </label>
          <input
            type="text"
            className="form-control"
            id="codigo"
            name="codigo"
            required
            value={codigo}
            onChange={handleChange}
            // onChange={(e) => setCodigo(e.target.value)}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="nombre" className="form-label">
            Nombre
          </label>
          <input
            type="text"
            className="form-control"
            id="nombre"
            name="nombre"
            required
            value={nombre}
            onChange={handleChange}
            // onChange={(e) => setNombre(e.target.value)}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="categoria" className="form-label">
            Categoria
          </label>
          <select
            id="categoria"
            name="categoria"
            className="form-select"
            required
            value={categoria}
            onChange={handleChange}
            // onChange={(e) => setCategoria(e.target.value)}
          >
            <option value="">Opciones...</option>
            <option value="Perros">Perros</option>
            <option value="Gatos">Gatos</option>
            <option value="Conejos">Conejos</option>
            <option value="Vacas">Vacas</option>
            <option value="Caballos">Caballos</option>
            <option value="Aves">Aves</option>
          </select>
        </div>
        <div className="col-md-3">
          <label htmlFor="unidadMedida" className="form-label">
            UN/Medida
          </label>
          <input
            type="text"
            className="form-control"
            id="unidadMedida"
            name="unidadMedida"
            required
            value={unidadMedida}
            onChange={handleChange}
            // onChange={(e) => setUnidadMedida(e.target.value)}
          />
        </div>
        <div className="col-md-3">
          <label htmlFor="valorCompra" className="form-label">
            Valor compra
          </label>
          <input
            type="text"
            className="form-control"
            id="valorCompra"
            name="valorCompra"
            required
            value={valorCompra}
            onChange={handleChange}
            // onChange={(e) => setValorCompra(e.target.value)}
          />
        </div>
        <div className="col-md-3">
          <label htmlFor="valorVenta" className="form-label">
            valorVenta
          </label>
          <input
            type="text"
            className="form-control"
            id="valorVenta"
            name="valorVenta"
            required
            value={valorVenta}
            onChange={handleChange}
            // onChange={(e) => setValorVenta(e.target.value)}
          />
        </div>
        <div className="col-md-3">
          <label htmlFor="cantidad" className="form-label">
            Cantidad
          </label>
          <input
            type="text"
            className="form-control"
            id="cantidad"
            name="cantidad"
            required
            value={cantidad}
            onChange={handleChange}
            // onChange={(e) => setCantidad(e.target.value)}
          />
        </div>
        <div className="col-12 pt-2">
          <button type="submit" className="btn btn-primary">
            {!id ? "Guardar" : "Actualizar"}
          </button>
          <Link to={"/admin/producto"}>
            <div className="float-end">
              <button type="button" className="btn btn-secondary">
                Back
              </button>
            </div>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AddProducto;
