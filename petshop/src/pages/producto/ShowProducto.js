import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/producto";

const ShowProducto = () => {
  const [formData, setFormData] = useState({
    nombre: "",
    codigo: "",
    image: "",
    unidadMedida: "",
    valorCompra: "",
    valorVenta: "",
    cantidad: "",
    categoria: "",
  });

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      showProducto(id);
    }
  }, [id]);

  /** manejo de inputs */
  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const showProducto = (id) => {
    axios.get(`${baseURL}/${id}`).then((response) => {
      setFormData(response.data);
      console.log("1", response.data);
    });
  };

  return (
    <div className="p-4">
      <h5>Detalles Producto</h5>
      <hr />
      <form className="row g-1 pt-2">
        <div className="col-md-4 p-2">
          <div>
            <img
              className="img-responsive img-thumbnail "
              id="image"
              src={formData.image}
              alt="..."
            />
          </div>
        </div>
        <div className="col-md-8">
          <div className="row">
            <div className="col-md-6">
              <label htmlFor="codigo" className="form-label">
                Codigo
              </label>
              <input
                type="text"
                className="form-control"
                id="codigo"
                name="codigo"
                disabled
                value={formData.codigo}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-6">
              <label htmlFor="nombre" className="form-label">
                Nombre
              </label>
              <input
                type="text"
                className="form-control"
                id="nombre"
                name="nombre"
                disabled
                value={formData.nombre}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-6">
              <label htmlFor="categoria" className="form-label">
                Categoria
              </label>
              <select
                id="categoria"
                name="categoria"
                className="form-select"
                disabled
                value={formData.categoria}
                onChange={handleChange}
              >
                <option value="">Opciones...</option>
                <option value="Perros">Perros</option>
                <option value="Gatos">Gatos</option>
                <option value="Gatos">Conejos</option>
                <option value="Gatos">Avicola</option>
              </select>
            </div>
            <div className="col-md-6">
              <label htmlFor="unidadMedida" className="form-label">
                UN/Medida
              </label>
              <input
                type="text"
                className="form-control"
                id="unidadMedida"
                name="unidadMedida"
                disabled
                value={formData.unidadMedida}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <label htmlFor="valorCompra" className="form-label">
                Valor compra
              </label>
              <input
                type="text"
                className="form-control"
                id="valorCompra"
                name="valorCompra"
                disabled
                value={formData.valorCompra}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <label htmlFor="valorVenta" className="form-label">
                valor Venta
              </label>
              <input
                type="text"
                className="form-control"
                id="valorVenta"
                name="valorVenta"
                disabled
                value={formData.valorVenta}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <label htmlFor="cantidad" className="form-label">
                cantidad
              </label>
              <input
                type="text"
                className="form-control"
                id="cantidad"
                name="cantidad"
                disabled
                value={formData.cantidad}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="col-12 pt-2">
          <Link to={"/admin/producto"}>
            <button type="submit" className="btn btn-primary">
              Back
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default ShowProducto;
