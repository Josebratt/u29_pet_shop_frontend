import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/proveedor";

const Proveedor = () => {
  const [proveedores, setProveedores] = useState([]);

  useEffect(() => {
    getProveedor();
  }, []);

    /** Validamos que el usuario este logueado */
    const authUser = () => {
      const user = JSON.parse(localStorage.getItem('token'));
  
      if (user && user.token) {
        return {Authorization: 'Bearer ' + user.token}
      } else {
        return {};
      }
    }


  /** lista de proveedores */
  const getProveedor = () => {
    axios.get(baseURL).then((response) => {
      setProveedores(response.data);
      console.log(response.data);
    });
  };

  /* eliminar proveedor */
  const deleteProveedor = (e, id) => { 
    e.preventDefault();
    axios.delete(`${baseURL}/${id}`, {headers: authUser()}).then(
      (borrado) => {
        if (borrado) {
          getProveedor();
        }
      }
    )
  }

  return (
    <div className="pt-4 p-2 table-responsive">
      <h2>
        Lista de proveedores
        <Link to={"/admin/proveedor/nuevo"}>
          <button type="button" className="btn btn-primary sm float-end">
            Nuevo
          </button>
        </Link>
      </h2>
      <hr />
      <br />
      <table className="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Nit</th>
            <th>Direccion</th>
            <th>Telefono</th>
            <th>Email</th>
            <th style={{ width: "15%" }}>acciones</th>
          </tr>
        </thead>
        <tbody>
          {proveedores.map((proveedor, index) => (
            <tr key={proveedor._id}>
              <td>{index + 1}</td>
              <td>{proveedor.nombre}</td>
              <td>{proveedor.numDoc}</td>
              <td>{proveedor.direccion}</td>
              <td>{proveedor.telefono}</td>
              <td>{proveedor.email}</td>
              <td>
                <div className="float-end">
                  <Link to={`/admin/proveedor/${proveedor._id}`}>
                    <button type="button" className="btn btn-success btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-eye `}></i>
                    </button>
                  </Link>
                  <Link to={`/admin/proveedor/update/${proveedor._id}`}>
                    <button type="button" className="btn btn-primary btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-pencil `}></i>
                    </button>
                  </Link>

                  <button type="button" className="btn btn-danger btn-sm" onClick={(e) => deleteProveedor(e, proveedor._id)}>
                    <i className={`fas fa-trash `}></i>
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Proveedor;
