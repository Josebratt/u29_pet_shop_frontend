import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/usuario/login";

const Login = () => {
    const navigate = useNavigate();

  const [user, setUser] = useState({
    email: "",
    clave: ""
  });

  const { email, clave } = user;

  const handleChange = (e) => {
    e.preventDefault();
    setUser({...user, [e.target.name]: e.target.value })
}

  const handleSubmit = async (e) => {
    e.preventDefault();
    const usuario = await axios.post(baseURL, user);
    navigate('/');
    window.location.reload(true);
    // .then((response) => {
    //     console.log(response.data);
    //     setUser(response.data);
    // });
    console.log(usuario);
    if (usuario) {
      window.localStorage.setItem('token',JSON.stringify(usuario.data));
    }

    setUser({email: '', clave: ''});
  };


  return (
    <div className="login-form">
      <form className="form" onSubmit={handleSubmit}>
        <h2 className="text-center">Iniciar Sesion</h2>
        <div className="form-group mb-2">
          <input
            type="text"
            className="form-control"
            placeholder="Email"
            required
            name="email"
            value={email}
            onChange={handleChange}
          />
        </div>
        <div className="form-group mb-2">
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            required
            name="clave"
            value={clave}
            onChange={handleChange}
          />
        </div>
        <div className="form-group mb-2">
          <button type="submit" className="btn btn-primary btn-block">
            Log in
          </button>
        </div>
      </form>
      <Link to={"/registro"} className="text-center">
        {" "}
        Crear una cuenta{" "}
      </Link>
    </div>
  );
};

export default Login;
