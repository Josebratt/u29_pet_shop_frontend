import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/usuario";

const ShowUsuario = () => {

    const [formData, setFormData] = useState({
        usuario:"",
        clave: "",
        nombres: "",
        role: "",
        apellidos: "",
        tipoDoc: "",
        numDoc: "",
        direccion: "",
        telefono: "",
        email: "",
        estado: ""
      });

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      showUsuario(id);
    }
  }, [id]);

  /** manejo de inputs */
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const showUsuario = (id) => {
    axios.get(`${baseURL}/${id}`).then((response) => {
        setFormData(response.data);
    });
  };

  return (
    <div className="p-5">
      <h5>Detalles Usuario</h5>
      <hr />
      <form className="row g-2 pt-3">
      <div className="col-md-4">
          <label htmlFor="usuario" className="form-label">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            id="usuario"
            name="usuario"
            disabled
            required
            value={formData.usuario}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="nombres" className="form-label">
            Nombres
          </label>
          <input
            type="text"
            className="form-control"
            id="nombre"
            name="nombre"
            disabled
            value={formData.nombres}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="apellidos" className="form-label">
            Apellidos
          </label>
          <input
            type="text"
            className="form-control"
            id="apellido"
            name="apellido"
            disabled
            value={formData.apellidos}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="tipoDoc" className="form-label">
            Tipo de Documento
          </label>
          <select
            id="tipoDoc"
            name="tipoDoc"
            className="form-select"
            disabled
            value={formData.tipoDoc}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="NIT">NIT</option>
            <option value="CC">CC</option>
          </select>
        </div>
        <div className="col-md-4">
          <label htmlFor="numDoc" className="form-label">
            Nro. Documento
          </label>
          <input
            type="text"
            className="form-control"
            id="numDocumento"
            name="numDoc"
            disabled
            value={formData.numDoc}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="telefono" className="form-label">
            Telefono
          </label>
          <input
            type="text"
            className="form-control"
            id="telefono"
            name="telefono"
            disabled
            value={formData.telefono}
            onChange={handleChange}
          />
        </div>
        <div className="col-12">
          <label htmlFor="direccion" className="form-label">
            Direccion
          </label>
          <input
            type="text"
            className="form-control"
            id="direccion"
            name="direccion"
            disabled
            value={formData.direccion}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            name="email"
            disabled
            value={formData.email}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="role" className="form-label">
            Rol
          </label>
          <select
            id="role"
            name="role"
            className="form-select"
            disabled
            value={formData.role}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="usuario">Usuario</option>
            <option value="admin">administrador</option>
            <option value="transportador">transportador</option>
          </select>
        </div>
        <div className="col-md-4">
          <label htmlFor="estado" className="form-label">
            Estado
          </label>
          <select
            id="estado"
            name="estado"
            className="form-select"
            disabled
            value={formData.estado}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="true">Activo</option>
            <option value="false">Inactivo</option>
          </select>
        </div>


        <div className="col-12">
          <Link to={"/admin/usuario"}>
            <button type="submit" className="btn btn-primary">
              Back
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default ShowUsuario;
