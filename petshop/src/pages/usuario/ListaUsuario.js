import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/usuario";

const ListaUsuario = () => {
  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    getUsuarios();
  }, []);

  /** lista de proveedores */
  const getUsuarios = () => {
    axios.get(baseURL).then((response) => {
        setUsuarios(response.data);
      console.log(response.data);
    });
  };

  /* eliminar proveedor */
//   const deleteProveedor = (e, id) => { 
//     e.preventDefault();
//     axios.delete(`${baseURL}/${id}`, {headers: authUser()}).then(
//       (borrado) => {
//         if (borrado) {
//             getUsuarios();
//         }
//       }
//     )
//   }

  return (
    <div className="pt-4 p-2 table-responsive">
      <h2>
        Lista de usuarios
        <Link to={"/admin/usuario/nuevo"}>
          <button type="button" className="btn btn-primary sm float-end">
            Nuevo
          </button>
        </Link>
      </h2>
      <hr />
      <br />
      <table className="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Rol</th>
            <th>Email</th>
            <th style={{ width: "15%" }}>acciones</th>
          </tr>
        </thead>
        <tbody>
          {usuarios.map((usuario, index) => (
            <tr key={usuario._id}>
              <td>{index + 1}</td>
              <td>{usuario.nombres}</td>
              <td>{usuario.apellidos}</td>
              <td>{usuario.role}</td>
              <td>{usuario.email}</td>
              <td>
                <div className="float-end">
                  <Link to={`/admin/usuario/${usuario._id}`}>
                    <button type="button" className="btn btn-success btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-eye `}></i>
                    </button>
                  </Link>
                  <Link to={`/admin/usuario/update/${usuario._id}`}>
                    <button type="button" className="btn btn-primary btn-sm" style={{ marginRight: "3px" }}>
                      <i className={`fas fa-pencil `}></i>
                    </button>
                  </Link>

                  {/* <button type="button" className="btn btn-danger btn-sm" onClick={(e) => deleteProveedor(e, usuario._id)}>
                    <i className={`fas fa-trash `}></i>
                  </button> */}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListaUsuario;