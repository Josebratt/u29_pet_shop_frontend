import React, { useState, useEffect } from "react";
import { useNavigate, Link, useParams } from "react-router-dom";
import axios from "axios";

const baseURL = "https://u29petshopbackend.herokuapp.com/usuario";

const AddUsuario = () => {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    usuario: "",
    clave: "",
    nombres: "",
    role: "",
    apellidos: "",
    tipoDoc: "",
    numDoc: "",
    direccion: "",
    telefono: "",
    email: "",
    estado: "",
  });

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      showUsuario(id);
    }
  }, [id]);

  /** Validamos que el usuario este logueado */
  const authUser = () => {
    const user = JSON.parse(localStorage.getItem("token"));

    if (user && user.token) {
      return { Authorization: "Bearer " + user.token };
    } else {
      return {};
    }
  };

  const showUsuario = (id) => {
    axios.get(`${baseURL}/${id}`).then((response) => {
      setFormData(response.data);
    });
  };

  const {
    usuario,
    nombres,
    apellidos,
    role,
    tipoDoc,
    numDoc,
    direccion,
    telefono,
    email,
    estado,
  } = formData;

  /** manejo de inputs */
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  /** manejo del submit */
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!id) {
      saveUsuario();
    } else {
      updateUsuario(id, formData);
    }
  };

  /** Guardar proveedor */
  const saveUsuario = () => {
    axios.post(baseURL, formData, { headers: authUser() }).then((response) => {
      setFormData(response.data);
    });
    navigate("/admin/usuario");
  };

  /** Actualizar proveedor */
  const updateUsuario = (id, formData) => {
    axios
      .put(`${baseURL}/${id}`, formData, { headers: authUser() })
      .then((response) => {
        setFormData(response.data);
      });
    navigate("/admin/usuario");
  };
  //
  return (
    <div className="p-5">
      <h5>{!id ? "Crear Usuario" : "Actualizar Usuario"}</h5>
      <hr />
      <form className="row g-2 pt-3" onSubmit={handleSubmit}>
        <div className="col-md-4">
          <label htmlFor="usuario" className="form-label">
            Usuario
          </label>
          <input
            type="text"
            className="form-control"
            id="usuario"
            name="usuario"
            required
            value={usuario}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="nombres" className="form-label">
            Nombres
          </label>
          <input
            type="text"
            className="form-control"
            id="nombres"
            name="nombres"
            required
            value={nombres}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="apellidos" className="form-label">
            Apellidos
          </label>
          <input
            type="text"
            className="form-control"
            id="apellidos"
            name="apellidos"
            required
            value={apellidos}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="tipoDoc" className="form-label">
            Tipo de Documento
          </label>
          <select
            id="tipoDoc"
            name="tipoDoc"
            className="form-select"
            required
            value={tipoDoc}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="NIT">NIT</option>
            <option value="CC">CC</option>
          </select>
        </div>
        <div className="col-md-4">
          <label htmlFor="numDoc" className="form-label">
            Nro. Documento
          </label>
          <input
            type="text"
            className="form-control"
            id="numDocumento"
            name="numDoc"
            required
            value={numDoc}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="telefono" className="form-label">
            Telefono
          </label>
          <input
            type="text"
            className="form-control"
            id="telefono"
            name="telefono"
            required
            value={telefono}
            onChange={handleChange}
          />
        </div>
        <div className="col-12">
          <label htmlFor="direccion" className="form-label">
            Direccion
          </label>
          <input
            type="text"
            className="form-control"
            id="direccion"
            name="direccion"
            required
            value={direccion}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            name="email"
            required
            value={email}
            onChange={handleChange}
          />
        </div>
        <div className="col-md-4">
          <label htmlFor="role" className="form-label">
            Rol
          </label>
          <select
            id="role"
            name="role"
            className="form-select"
            value={role}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="usuario">Usuario</option>
            <option value="admin">administrador</option>
            <option value="transportador">transportador</option>
          </select>
        </div>
        <div className="col-md-4">
          <label htmlFor="estado" className="form-label">
            Estado
          </label>
          <select
            id="estado"
            name="estado"
            className="form-select"
            value={estado}
            onChange={handleChange}
          >
            <option value="">Opciones...</option>
            <option value="true">Activo</option>
            <option value="false">Inactivo</option>
          </select>
        </div>

        <div className="col-12 pt-2">
          <button type="submit" className="btn btn-primary">
            {!id ? "Guardar" : "Actualizar"}
          </button>
          <Link to={"/admin/usuario"}>
            <div className="float-end">
              <button type="button" className="btn btn-secondary">
                Back
              </button>
            </div>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AddUsuario;
