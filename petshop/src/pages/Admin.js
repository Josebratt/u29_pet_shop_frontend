import React, { useState } from 'react';
import { Link, Outlet, useNavigate } from "react-router-dom";
import "../admin.css";

const Admin = () => {
  const [show, setShow] = useState(false);

  const navigate = useNavigate();

  const logout = (e) => {
    e.preventDefault();
    navigate('/');
    window.location.reload(true);
    return Promise.resolve().then(
        window.localStorage.removeItem('token')
    );
  }

  return (
    <div className="admin">
    <main className={show ? 'space-toggle' : null}>
      <header className={`header ${show ? 'space-toggle' : null}`}>
        <div className='header-toggle' onClick={() => setShow(!show)}>
          <i className={`fas fa-bars ${show ? 'fa-solid fa-xmark' : null}`}></i>
        </div>
      </header>

      <aside className={`sidebar ${show ? 'show' : null}`}>
        <nav className='nav'>
          <div>
            <Link to='/admin' className='nav-logo'>
              <i className={`fas fa-home-alt nav-logo-icon`}></i>
              <span className='nav-logo-name text-white'>PetShop</span>
            </Link>

            <div className='nav-list'>
              <Link to='/admin' className='nav-link active'>
                <i className='fas fa-tachometer-alt nav-link-icon'></i>
                <span className='nav-link-name'>Dashboard</span>
              </Link>
              <Link to='/admin/proveedor' className='nav-link'>
                <i className='fas fa-truck nav-link-icon'></i>
                <span className='nav-link-name'>Proveedor</span>
              </Link>
              <Link to='/admin/producto' className='nav-link'>
                <i className='fas fa-suitcase nav-link-icon'></i>
                <span className='nav-link-name'>Producto</span>
              </Link>
              <Link to='/admin' className='nav-link'>
                <i className='fas fa-wallet nav-link-icon'></i>
                <span className='nav-link-name'>Facturas</span>
              </Link>
              <Link to='/admin/usuario' className='nav-link'>
                <i className='fas fa-users nav-link-icon'></i>
                <span className='nav-link-name'>Usuarios</span>
              </Link>
              <Link to='/admin' className='nav-link'>
                <i className='fas fa-tools nav-link-icon'></i>
                <span className='nav-link-name'>Configuracion</span>
              </Link>
            </div>
          </div>

          <Link to='/' className='nav-link'>
            <i className='fas fa-sign-out nav-link-icon'></i>
            <span className='nav-link-name' onClick={(e) => logout(e)} >Logout</span>
          </Link>
        </nav>
      </aside>
      <div>
        <Outlet></Outlet>
      </div>
    </main>
    </div>
  );
};

export default Admin;
