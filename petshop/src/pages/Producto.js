import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const Producto = () => {
  const baseURL = "https://u29petshopbackend.herokuapp.com/producto";

  const [productos, setProductos] = useState([]);

  useEffect(() => {
    getProducto();
  }, []);

  /** lista de productos */
  const getProducto = () => {
    axios.get(baseURL).then((response) => {
      setProductos(response.data);
    });
  };

  return (
    <div>
      <div className="banner mt-4">
        <div className="row">
          <div className="col-5 offset-1 banner-text">
            <h1>The Best Products</h1>
            <h2>for Every Animal</h2>
          </div>
          <div className="col-5 banner-image animate__animated animate__slideInLeft">
            <img src="./banner-image.svg" alt="" />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row mt-4">
          {productos.map((producto) => (
            <div
              className="card col-md-3 m-2"
              style={{ maxWidth: "16rem" }}
              key={producto._id}
            >
              {/* <p className="hide">key={producto._id}</p> */}

              <img
                src={producto.image}
                className="card-img-top img-responsive"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">{producto.nombre}</h5>
                <p className="card-text">{producto.valorVenta}</p>
                <Link>
                  <button className="btn btn-primary"> Ver producto</button>
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Producto;
