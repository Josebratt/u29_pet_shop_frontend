import React, { useState, useEffect } from "react";
import axios from "axios";

const Dashboard = () => {
  const baseURL = "https://u29petshopbackend.herokuapp.com";

  const [productos, setProductos] = useState([]);
  const [usuarios, setUsuarios] = useState([]);
  const [proveedores, setProveedores] = useState([]);

  useEffect(() => {
    getProducto();
    getUsuario();
    getProveedor();
  }, []);

  /** lista de productos */
  const getProducto = () => {
    axios.get(`${baseURL}/producto`).then((response) => {
      setProductos(response.data);
    });
  };

  /** lista de usuarios */
  const getUsuario = () => {
    axios.get(`${baseURL}/usuario`).then((response) => {
      setUsuarios(response.data);
    });
  };

   /** lista de proveedores */
   const getProveedor = () => {
    axios.get(`${baseURL}/proveedor`).then((response) => {
      setProveedores(response.data);
    });
  };

  return (
    <>
      <div className="row pt-3">
        <div className="col-md-3">
          <div className="d-products">
            <div className="d-item">
              <div>
                <i className="fas fa-suitcase"></i>
              </div>
              <div className="row">
                <span className="name">Productos</span>
                <span className="number">{productos.length}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="d-users">
            <div className="d-item">
              <div>
                <i className="fas fa-users"></i>
              </div>
              <div className="row">
                <span className="name">Usuarios</span>
                <span className="number">{usuarios.length}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="d-supplies">
            <div className="d-item">
              <div>
                <i className="fas fa-truck"></i>
              </div>
              <div className="row">
                <span className="name">Proveedores</span>
                <span className="number">{proveedores.length}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;