import React from "react";

const Footer = () => {
    let url="#";

  return (
    <footer className="footer">
        <div className="footer-bottom">
            <p>Copyright &copy; 2022 UIS <a href={url}> React FullStack Developer</a></p>
        </div>
    </footer>
  );
};

export default Footer;