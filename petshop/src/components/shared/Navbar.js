import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

const Navbar = () => {

    const navigate = useNavigate();

    let url = "#";

    const [user, setUser] = useState([]);

    useEffect(() => {
        const token = window.localStorage.getItem('token');
        if (token) {
            const user = JSON.parse(token);
            console.log(user);
            setUser(user.JSON);
        }
    }, []);

    const logout = (e) => {
        e.preventDefault();
        navigate('/');
        window.location.reload(true);
        return Promise.resolve().then(
            window.localStorage.removeItem('token')
        );
    }


    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container">
                    <Link to={"/"} className="navbar-brand" >Pet Shop </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link to={"/"} className="nav-link active" aria-current="page">Home </Link>
                            </li>
                            {user ?
                                <li className="nav-item">
                                    <Link to={"/login"} className="nav-link">
                                        <i className="fas fa-user"></i> Login
                                    </Link>
                                </li> :
                                <>
                                    <li className="nav-item">
                                        <Link to={"/admin"} className="nav-link">
                                            Admin
                                        </Link>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href={url} role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            usuario
                                        </a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item" href={url}>Action</a></li>
                                            <li><hr className="dropdown-divider" /></li>
                                            <li><a className="dropdown-item" href={url} onClick={(e) => logout(e)}>Logout</a></li>
                                        </ul>
                                    </li>
                                </>
                            }

                        </ul>
                    </div>
                </div>
            </nav>
        </div>

    );
};

export default Navbar;